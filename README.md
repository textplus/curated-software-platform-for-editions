# Curated Software Platform for Editions

## Description
The Curated Software Platform for Editions will provide an overview of available research software applied in the scholarly editions domain. It aims to facilitate the curation process for research software metadata.
Target group are scholars working on editions.

A first version will be available Fall 2023.

## Current Version of the Data Model (WIP) 
(specified as GraphQL Schema)

```graphql

type EditionSoftware {
    id: ID!
    name: String!
    description: String!
    categories: [EditionPhase]!
    keywords: [String]!
    version: String!
    sourceCode: [URL]
    availableAt: [URL]!
    softwareLicenseType: [LicenseType]
    usedInEditions: [Edition]
    alternatives: [EditionSoftware]
    collaborators: [Collaborator]
    citation: CFFyaml!
    userDocumentation: [URL]
    technicalDetails: TechnicalDetails
    support: 
    reviews: [URL]
    approved: bool!
    lastChanged: Timestamp!
}

enum EditionPhase {
    Workflow/Datenmanagement
    Erfassung/Digitalisierung
    Transkription
    Modelling/Encoding
    Erschließung/Anreicherung
    Processing
    Kollation
    Analysen/Visualisierungen
    Verknüpfung
    Hosting
    Präsentation/Publikation
    Archivierung/Nachhaltigkeit
}

enum LicenseType {
    freeAcademicUse
    monthly
    ...
}

enum OperationInterface {
    UI
    REST_API
    GraphQL_API
    CLI
    ...
}

type Identifier {
    idCode: String!
    domain: URL!
}

type Edition {
    name: String!
    id: [Identifier]
    availableAt: [URL]
}

type VocabularyTerm {
    termURI: URI
    termId: ID
    vocabularyId: ID
}

type TechnicalDetails {
    technicalDocumentation: [URL]
    prerequisites: [String]
    operationInterfaces: [operationInterface]
}

type Collaborator {
    ids: [Identifier]
    name: String!
}

scalar URL

scalar URI

scalar URL

scalar Timestamp
```

